##  Road Filter App - extract the noise out from your road trip itinerary maps

## Motivation

This app reads data from a GNSS output file and filters it using a FIR filter. 
The main idea is to changed the cutoff frequency of a FIR filter using a UI slider,
while looking at the visual result on the displayed filtered data.
In such way you may learn the best FIR coefficients matching *your* expectations.
The program uses the Remez optimal FIR length filter design algorithm.
See https://en.wikipedia.org/wiki/Remez_algorithm


## Installation

You need python 3, so if not yet installed you may use this link to install:

    https://www.python.org/downloads/


## Usage

From inside a new and empty directory execute:

    mkdir -p /tmp/xyz
    git clone https://claudio_ortega@bitbucket.org/claudio_ortega/bici-dsp.git

Create a python environment inside

    cd bici-dsp
    python -m venv venv
    cd venv
    source bin/activate


Install libraries inside the virtual environment

    python -m pip install -U matplotlib numpy scipy

Create some handy aliases

    alias python=python3
    alias pip=pip3

Execute the program

    cd ../venv
    source bin/activate
    cd ../src
    python road_filter.py <file_path> <column_zero_based>

ie:

    python road_filter.py knights.csv 5

Note: In ubuntu you may get an error related to python not finding some Qt libraries. If so, do:

    sudo apt-get install python3-tk


The input file should either come from the gpxFileTool, or otherwise, have the same format and meaning.
The app nees the column in the file which corresponds to the road altitude obtained via GNSS.

You can control any of the following:

    - the zoomed-in signal segment to get filtered
    - the window length for the filter
    - the window type for the filter, any of: {rectangular, blackman, bartlett, hamming, cosine}

When closing, the app will produce two files under the directory ./windows; 
one file with the filter coefficients, and a second file with the input signal filtered 
by a FIR with those coefficients.


![](./ui-screen.png) 

