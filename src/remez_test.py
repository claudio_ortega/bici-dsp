import numpy as np
from scipy import signal
import matplotlib.pyplot as plt


def test_lowpass():
    fs = 1.000
    cutoff_freq = 0.150
    trans_width = 0.010
    fir_coefs = signal.remez(
        maxiter=30,
        numtaps=251,
        bands=[0, cutoff_freq, cutoff_freq + trans_width, 0.5*fs],
        desired=[1, 0],
        fs=fs,
    )
    freq, h = signal.freqz(fir_coefs, [1], worN=2000, fs=fs)
    plot_freq_response(freq, h, "Low-pass Filter")
    plt.show()


def plot_freq_response(freq, h, title):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(freq, 20*np.log10(np.abs(h)))
    ax.set_ylim(-125, 5)
    ax.grid(True)
    ax.set_xlabel('Frequency (Hz)')
    ax.set_ylabel('Gain (dB)')
    ax.set_title(title)


if __name__ == '__main__':
    test_lowpass()
