import os
from typing import List

import numpy as np
import scipy.fftpack
import scipy.signal
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.widgets import Slider
import sys


def extract_f_t_from_input_data(f_t_from_file):
    first_index = 0
    last_index = len(f_t_from_file)
    span_index = last_index-first_index
    t = np.linspace(first_index, last_index, span_index)
    f_t = f_t_from_file[first_index:last_index]
    return t, f_t


def compute_freq_response(t, f_t):
    if len(t) != len(f_t):
        raise AssertionError("len(t) != len(f_t)")

    first_index = 0
    last_index = len(t)
    span_index = last_index-first_index

    f = scipy.fftpack.fftfreq(span_index, 1.0)
    f_omega = np.abs(scipy.fftpack.fft(f_t))

    f_omega = f_omega + 1e-30
    f_omega_log = 20 * np.log10(f_omega)
    f_omega_log -= np.max(f_omega_log)

    f = np.roll(f, span_index//2)
    f_omega_log = np.roll(f_omega_log, span_index//2)

    return f, f_omega_log


def init_plot(all_signal_axis, zoomed_axis, all_spectrum_axis):
    all_signal_axis.grid(True)
    line_f_t, = all_signal_axis.plot([], [], color='royalblue')
    line_f_t_markers_left, = all_signal_axis.plot([], [], color='gray', linewidth=1 )
    line_f_t_markers_right, = all_signal_axis.plot([], [], color='gray', linewidth=1 )

    all_spectrum_axis.grid(True)
    line_f_w_filtered_zoomed, = all_spectrum_axis.plot([], [], color='darkolivegreen')

    zoomed_axis.grid(True)
    line_f_t_zoomed, = zoomed_axis.plot([], [], color='gray')
    line_f_filtered_zoomed, = zoomed_axis.plot([], [], color='firebrick')

    return \
        line_f_t, line_f_w_filtered_zoomed, line_f_t_zoomed, line_f_filtered_zoomed, \
        line_f_t_markers_left, line_f_t_markers_right


def set_legends(all_signal_axis, zoomed_axis, all_spectrum_axis):
    patch_1 = mpatches.Patch(color='royalblue', label='all_signal')
    all_signal_axis.legend(handles=[patch_1])

    patch_2 = mpatches.Patch(color='darkolivegreen', label='spectrum')
    all_spectrum_axis.legend(handles=[patch_2])

    index_left, index_right = compute_index_zoom_interval(center_slider.val, width_slider.val, len_data)

    patch_3a = mpatches.Patch(color='gray', label="original [" + str(index_left) + "," + str(index_right) + "]")
    patch_3b = mpatches.Patch(color='firebrick', label=f"cutoff @ {cutoff_freq_slider.val:.3f} Hz")
    zoomed_axis.legend(handles=[patch_3a, patch_3b])


def compute_index_zoom_interval( center, width, index_width ):
    normalized_center = center / index_width
    normalized_width = width / index_width

    tmp_left = (normalized_center - 0.5 * normalized_width)
    tmp_right = (normalized_center + 0.5 * normalized_width)

    if tmp_left < 0.0:
        tmp_left = 0.0
        tmp_right = normalized_width

    if tmp_right > 1.0:
        tmp_right = 1.0
        tmp_left = 1.0 - normalized_width

    index_left = max(index_width * tmp_left, 0)
    index_right = min(index_width * tmp_right, index_width-1)

    return int(index_left), int(index_right)


def with_margins(a, b, margin):
    delta = b-a
    if delta < 1e-6:
        return -1, 1
    return a-margin*delta, b+margin*delta


def get_fir_taps(cutoff_freq: float) -> List[float]:
    fs = 1.000
    trans_width = 0.010
    return scipy.signal.remez(
        maxiter=30,
        numtaps=251,
        bands=[0, cutoff_freq, cutoff_freq+trans_width, 0.5*fs],
        desired=[1, 0],
        fs=fs,
    )


def plot_zoom_marker(line, x, bottom, top, d):
    line.set_xdata(np.array([x+d, x, x, x+d]))
    line.set_ydata(np.array([top, top, bottom, bottom]))


def half_length(length: int) -> int:
    if length % 2 == 0:
        raise AssertionError("length should be odd")
    return int((length-1) / 2)


def plot(
        line_f_t, 
        line_f_w_filtered_zoomed, 
        line_f_t_zoomed, 
        line_f_t_filtered_zoomed, 
        line_f_t_marker_left, 
        line_f_t_marker_right, 
        t, 
        f_t,
):
    index_left, index_right = compute_index_zoom_interval(center_slider.val, width_slider.val, len_data)

    t_zoomed = t[index_left:index_right+1]
    f_t_zoomed = f_t[index_left:index_right+1]

    cutoff_freq = cutoff_freq_slider.val
    window_taps = get_fir_taps(cutoff_freq)
    fir_length = len(window_taps)
    half_fir_length = half_length(fir_length)

    f_t_filtered_zoomed = scipy.signal.lfilter(window_taps, [1], f_t_zoomed)
    f_t_filtered_zoomed = np.roll(f_t_filtered_zoomed, -half_fir_length)

    line_f_t.set_xdata(t)
    line_f_t.set_ydata(f_t)

    all_signal_axis.set_xlim(with_margins(np.amin(t), np.amax(t), 0.01))
    all_signal_axis.set_ylim(with_margins(np.amin(f_t), np.amax(f_t), 0.05))

    plot_zoom_marker( line_f_t_marker_left, index_left, np.amin(f_t), np.amax(f_t), 20)
    plot_zoom_marker( line_f_t_marker_right, index_right, np.amin(f_t), np.amax(f_t), -20)

    line_f_t_zoomed.set_xdata(t_zoomed)
    line_f_t_zoomed.set_ydata(f_t_zoomed)

    if half_fir_length == 0:
        raise AssertionError("half_fir_length == 0")

    # avoid to plot half_fir_length samples on both sides
    clip_length = half_fir_length
    t_filtered_zoomed = t_zoomed[clip_length:-(clip_length+1)]
    f_t_filtered_zoomed = f_t_filtered_zoomed[clip_length:-(clip_length+1)]

    line_f_t_filtered_zoomed.set_xdata(t_filtered_zoomed)
    line_f_t_filtered_zoomed.set_ydata(f_t_filtered_zoomed)

    zoomed_axis.set_xlim(with_margins(index_left, index_right + 1, 0.01))

    min_1 = np.amin(f_t_zoomed)
    max_1 = np.amax(f_t_zoomed)

    # a precondition for any further computation is that len(f_t_filtered_zoomed) > 0
    if len(f_t_filtered_zoomed) == 0:
        return

    min_2 = np.amin(f_t_filtered_zoomed)
    max_2 = np.amax(f_t_filtered_zoomed)

    zoomed_axis.set_ylim(with_margins(min(min_1, min_2), max(max_1, max_2), 0.05))

    zeros_length = 100*fir_length
    half_zeros_length = int(zeros_length/2)
    t_taps = np.linspace(
        -half_fir_length-zeros_length/2,
        half_fir_length+zeros_length/2,
        fir_length+zeros_length)
    f, h_log = compute_freq_response(
        t_taps,
        np.concatenate((np.zeros(half_zeros_length), window_taps, np.zeros(half_zeros_length))))

    line_f_w_filtered_zoomed.set_xdata(f)
    line_f_w_filtered_zoomed.set_ydata(h_log)
    all_spectrum_axis.set_xlim(
        with_margins(np.amin(f), np.amax(f), 0.01))
    all_spectrum_axis.set_ylim(
        with_margins(max(-125, np.amin(h_log)),
                     min(5, np.amax(h_log)),
                     0.05))
    fig.canvas.draw_idle()


def create_chirp(
    len_data,
    chirp_center,
    chirp_width,
    t_0,
):
    w_0 = 6.28/t_0
    window_data = np.zeros(len_data)
    first_chirp_index = int(chirp_center - chirp_width / 2)
    last_chirp_index = int(chirp_center + chirp_width / 2)
    window_data[first_chirp_index:last_chirp_index] = 1
    t = np.linspace(0, len_data-1, len_data)
    return window_data * np.sin(w_0 * t)


if __name__ == '__main__':
    test_with_chirp = False

    if test_with_chirp:
        chirp_200 = create_chirp(10000, 2000, 200, 200)
        chirp_100 = create_chirp(10000, 3000, 200, 100)
        chirp_50 = create_chirp(10000, 4000, 200, 50)
        chirp_20 = create_chirp(10000, 5000, 200, 20)
        chirp_10 = create_chirp(10000, 6000, 200, 10)

        input_data = chirp_200 + chirp_100 + chirp_50 + chirp_20 + chirp_10
    else:
        if len(sys.argv) != 3:
            print("usage:" + sys.argv[0] + " file_path column_zero_based")
            sys.exit()
        data_file_name = sys.argv[1]
        column = int(sys.argv[2])
        data_from_file = np.loadtxt(data_file_name, skiprows=1, delimiter=",")
        input_data = data_from_file[:, column]

    len_data = len(input_data)

    t, f_t = extract_f_t_from_input_data(input_data)

    fig, (all_signal_axis, zoomed_axis, all_spectrum_axis) = plt.subplots(nrows=3, ncols=1, figsize=(10, 9))

    # the plots are defined to live below the sliders
    plt.subplots_adjust(left=0.15, top=0.78, right=0.9, bottom=0.05)

    line_f_t, line_f_w_filtered_zoomed, \
    line_f_t_zoomed, line_f_t_filtered_zoomed, \
    line_f_t_markers_left, line_f_t_markers_right = \
        init_plot(all_signal_axis, zoomed_axis, all_spectrum_axis)

    # center for the spectrum analysis
    center_slider = Slider(
        ax=plt.axes((0.15, 0.95, 0.75, 0.025), facecolor="white"),
        label='zoomed center',
        valmin=0,
        valmax=len_data,
        valinit=len_data/2)

    # width for the spectrum analysis
    width_slider = Slider(
        ax=plt.axes((0.15, 0.90, 0.75, 0.025), facecolor="white"),
        label='zoomed width',
        valmin=0,
        valmax=len_data,
        valinit=len_data/2)

    # width for the FIR length
    cutoff_freq_slider = Slider(
        ax=plt.axes((0.15, 0.85, 0.75, 0.025), facecolor="white"),
        label='cutoff frequency',
        valmin=0.001,
        valmax=0.499,
        valinit=0.15,
        valstep=0.001)

    def slider_cb_1(_: int):
        plot_and_set_legends()

    def plot_and_set_legends():
        plot(
            line_f_t, line_f_w_filtered_zoomed,
            line_f_t_zoomed, line_f_t_filtered_zoomed,
            line_f_t_markers_left, line_f_t_markers_right,
            t,
            f_t,
        )
        set_legends(all_signal_axis, zoomed_axis, all_spectrum_axis)

    def generate_filtered_output_files():
        cutoff_freq = float(cutoff_freq_slider.val)
        window_taps = get_fir_taps(cutoff_freq)
        fir_length = len(window_taps)
        half_fir_length = half_length(fir_length)
        f_t_filtered = scipy.signal.lfilter(window_taps, [1], f_t)
        f_t_filtered_rolled = np.roll(f_t_filtered, -half_fir_length)
        f_t_filtered_rolled_tapped = np.concatenate ( (
            np.zeros( half_fir_length ),
            f_t_filtered_rolled[half_fir_length+1:-half_fir_length],
            np.zeros( half_fir_length + 1 )) )

        windows_dir_name = "windows"
        windows_dir_path = os.path.join(os.getcwd(), windows_dir_name)
        if not os.path.isdir(windows_dir_path):
            os.mkdir(windows_dir_path)

        fir_taps_file_path = os.path.join(
            windows_dir_name,
            "window_taps_%.3f.csv" % cutoff_freq_slider.val)
        np.savetxt(fir_taps_file_path, window_taps, delimiter=",", fmt='%.18e')
        print("filter taps file path: " + fir_taps_file_path)

        filtered_signal_file_path = os.path.join(
            windows_dir_name,
            "filtered_out_%.3f.csv" % cutoff_freq_slider.val)
        np.savetxt(filtered_signal_file_path, f_t_filtered_rolled_tapped, delimiter=",", fmt='%.18e')
        print("filtered signal file path: " + filtered_signal_file_path)

    # register the update function with each slider
    center_slider.on_changed(slider_cb_1)
    width_slider.on_changed(slider_cb_1)
    cutoff_freq_slider.on_changed(slider_cb_1)

    plot_and_set_legends()
    plt.show()

    generate_filtered_output_files()

