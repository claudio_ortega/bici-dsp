1. install conda on macOS

    https://repo.anaconda.com/archive/Anaconda3-2020.11-MacOSX-x86_64.pkg

    Test:
      python --version 
        Python 3.8.8
    
      conda --version
        conda 4.9.2

      jupyter --version
        (base) 10:45:53$ jupyter --version 
        jupyter core     : 4.6.3
        jupyter-notebook : 6.1.4
        qtconsole        : 4.7.7
        ipython          : 7.19.0
        ipykernel        : 5.3.4
        jupyter client   : 6.1.7
        jupyter lab      : 2.2.6
        nbconvert        : 6.0.7
        ipywidgets       : 7.5.1
        nbformat         : 5.0.8
        traitlets        : 5.0.5
        (base) 12:05:37$ 


2. create/list environments

    conda create --name <env>
   
    conda env list
   
    (clip)
    coursera-self-driving-cars  *  /Users/cortega/.conda/envs/coursera-self-driving-cars
    base                           /Users/cortega/opt/anaconda3


3. change to one environment

    conda activate <env>

4. List packages already installed in the activated environment

    conda list

5. install a package in an environment

    conda install [-c conda-forge] <package>
    ie:
    conda install numpy
    conda install matplotlib
    conda install scipy

6. Update conda
    
    conda update -n base -c defaults conda


References
[1] https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html


